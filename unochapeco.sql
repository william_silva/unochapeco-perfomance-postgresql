SELECT pg_reload_conf();
SELECT pg_stat_reset();

DROP TABLE IF EXISTS t;         -- 10 registros
DROP TABLE IF EXISTS tt;        -- 100 registros
DROP TABLE IF EXISTS ttt;       -- 1.000 registros
DROP TABLE IF EXISTS tttt;      -- 10.000 registros
DROP TABLE IF EXISTS ttttt;     -- 100.000 registros
DROP TABLE IF EXISTS tttttt;    -- 1.000.000 registros
DROP TABLE IF EXISTS ttttttt;   -- 10.000.000 registros


CREATE TABLE t AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,10) i;

CREATE TABLE tt AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,100) i;

CREATE TABLE ttt AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,1000) i;

CREATE TABLE tttt AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,10000) i;

CREATE TABLE ttttt AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,100000) i;

CREATE TABLE tttttt AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,1000000) i;


CREATE TABLE ttttttt AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    i + random() * 100 nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,10000000) i;


CREATE TABLE toast_example AS
SELECT 
    i, 
    nullif(round(random() * 100),100) n,
    (i + random() * 100)::numeric nn,
    chr((round(random() * 94) + 32)::int) c, 
    repeat(chr((round(random() * 94) + 32)::int), round(random() * 20)::int) cc,
    (current_date + round(random()*365) * '1 day'::interval)::date d,
    current_timestamp + random()*365 * '1 day'::interval dd
FROM generate_series(1,10) i;


EXPLAIN ANALYZE 
SELECT * 
  FROM t
  JOIN tt ON tt.i = t.i
  JOIN ttt ON ttt.i = tt.i
  
EXPLAIN ANALYZE 
SELECT * 
  FROM ttt
  JOIN t ON t.i = ttt.i
  JOIN tt ON tt.i = t.i
  

EXPLAIN ANALYZE 
SELECT * 
  FROM t
  LEFT JOIN tt ON tt.i = t.i
  LEFT JOIN ttt ON ttt.i = tt.i
  
EXPLAIN ANALYZE 
SELECT * 
  FROM ttt
  LEFT JOIN t ON t.i = ttt.i
  LEFT JOIN tt ON tt.i = t.i
  
  
ANALYZE t;
ANALYZE tt;
ANALYZE ttt;
  
  
EXPLAIN ANALYZE 
WITH qualquer_nome AS (
    SELECT ttt.*
      FROM ttt 
      JOIN tt ON tt.i = ttt.i
), x AS (
    SELECT * FROM qualquer_nome
)
SELECT * 
  FROM qualquer_nome
  JOIN t ON t.i = qualquer_nome.i
  
EXPLAIN ANALYZE 
SELECT * FROM tttt 
 WHERE COALESCE(n, 0) != COALESCE(i, 0) 
  
  
EXPLAIN ANALYZE 
 SELECT * FROM tttt
  WHERE n IS DISTINCT FROM i;
  